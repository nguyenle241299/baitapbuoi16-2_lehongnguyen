// Bài 5
document.getElementById("btnPrintPrimes").onclick = function() {
    // input
    var input = document.getElementById("nhapSo").value * 1;

    // output

    var reSult = '';

    for(var count = 2; count <= input; count++) {
        // Kiểm tra số nguyên tố
        var checkSNT =  true
        for(var i = 2; i <= Math.sqrt(count); i++) {
            if(count % i == 0) {
                checkSNT = false;
                break;
            }
        }
        if(checkSNT) {
            reSult += count + ' ';
        }
    }
    document.getElementById("result_5").innerHTML = reSult;

}
